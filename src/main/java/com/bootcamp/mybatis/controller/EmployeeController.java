/**
 * Globe FinTech Innovations, Inc.
 * Copyright (c) 2004-2022 All Rights Reserved.
 */
package com.bootcamp.mybatis.controller;

import com.bootcamp.mybatis.dao.EmployeeMapper;
import com.bootcamp.mybatis.entity.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.List;

/**
 * @author Amitabh Biswal
 * @version $Id: EmployeeController.java, v 0.1 2022-10-27 10:50 AM Amitabh Biswal Exp $$
 */
@RestController
public class EmployeeController {

    @Autowired
    EmployeeMapper employeeMapper;

    @GetMapping("/employees")
    public List<Employee> getEmployeeDetails() throws IOException {

        Reader reader = Resources.getResourceAsReader("configuration.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        SqlSession session = sqlSessionFactory.openSession();

        Employee employee = (Employee) session.selectList("Employee.getAllEmployees", 2);


        return employeeMapper.getAllEmployees();
    }
}