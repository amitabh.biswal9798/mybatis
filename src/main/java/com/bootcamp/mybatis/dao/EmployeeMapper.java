/**
 * Globe FinTech Innovations, Inc.
 * Copyright (c) 2004-2022 All Rights Reserved.
 */
package com.bootcamp.mybatis.dao;
import com.bootcamp.mybatis.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Amitabh Biswal
 * @version $Id: EmployeeMapper.java, v 0.1 2022-10-27 11:25 AM Amitabh Biswal Exp $$
 */
@Mapper
public interface EmployeeMapper {
    public void saveEmployee(Employee employee);
    public void updateEmployee(Employee employee);
    public void deleteEmployee(int employeeId);
    public List<Employee> getAllEmployees();
    public Employee findById(int employeeId);
}